package produto;

public class Produto {
	
	private int id;//primary key
	private String nome;
	private String descricao;
	private Double valor;
	private float qtde;

	public Produto() {
		this.valor = new Double(0);
		this.nome = new String();
	}
	
	public Produto(String nome, Double valor) {
		this();
		this.nome = nome;
		this.valor = valor;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public int getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public float getQtde() {
		return qtde;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setCodigo(int codigo) {
		this.id = codigo;
	}

	public void setQtde(float qtde) {
		this.qtde = qtde;
	}

	public void imprimir() {
		System.out.println("==========Produto==========");
		System.out.println("Codigo: "+this.getId());
		System.out.println("Nome: "+this.getNome());
		System.out.println("Descrição: "+this.descricao);
		System.out.println("Valor: "+this.getValor());
		System.out.println("Quantidade: "+this.getQtde());
		System.out.println("===========================");
	}

}
