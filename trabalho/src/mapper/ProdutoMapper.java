package mapper;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.type.JdbcType;

import produto.Produto;

/**
 * Classe (interface) Modelo que contém implementação dos comandos que serão
 * executados na base de dados.
 *
 */
public interface ProdutoMapper {

    /**
     * Define como será realizado o insert na base dados.
     *
     * @Insert indica a anotação do MyBatis que realizará o comando insert
     * conforme sintaxe do SQL. #{} indica um parametro.
	 *
     */
    @Insert("INSERT INTO "
            + "produto(nome, descricao, valor, qtde)"
            + "	VALUES(#{nome}, #{descricao}, "
            + "#{valor}, #{qtde})")
    void insertProduto(Produto produto);
    
    @Select("SELECT * FROM produto WHERE id = #{id}")
            @Results({
                @Result(property="id", column="id", id=true),
                @Result(property="nome", column="nome"),
                @Result(property="valor", column="valor"),
                @Result(property="qtde", column="qtde"),
                @Result(property="descricao", column="descricao"),
            })
    Produto selectById(int id);

    @Update("UPDATE produto SET nome = #{nome} where id = #{id}")
    void updateNome(Produto produto);
    
    @Delete("DELETE FROM produto WHERE id = #{id}")
    void deleteProduto(int id);

}
