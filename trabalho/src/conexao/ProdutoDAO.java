package conexao;

import java.util.ArrayList;

import org.apache.ibatis.session.SqlSession;

import produto.Produto;
import mapper.ProdutoMapper;

/**
 * Classe Modelo para gerenciar as ações referentes ao banco de dados para a
 * entidade produto; faz referencia entre entidade e objeto (Produto).
 *
 */
public class ProdutoDAO {

    /**
     * Inserir produto na base dados. Utilizar a classe ProdutoMapper do pacote
     * mapper para realizar ação no banco de dados
     *
     */
    public void save(Produto produto) {
        SqlSession session = Conexao.getSqlSessionFactory().openSession();
        ProdutoMapper mapper = session.getMapper(ProdutoMapper.class);
        mapper.insertProduto(produto);
        session.commit();
        session.close();
    }

    public Produto selectProdutoById(Integer id) {
        SqlSession session = Conexao.getSqlSessionFactory().openSession();
        ProdutoMapper mapper = session.getMapper(ProdutoMapper.class);
        Produto produto = mapper.selectById(id);
        session.close();
        return produto;
    }

    public void updateDescricao(Produto produto) {
        SqlSession session = Conexao.getSqlSessionFactory().openSession();
        ProdutoMapper mapper = session.getMapper(ProdutoMapper.class);
        mapper.updateNome(produto);
        session.commit();
        session.close();
    }

    public void deleteProduto(Integer id) {
        SqlSession session = Conexao.getSqlSessionFactory().openSession();
        ProdutoMapper mapper = session.getMapper(ProdutoMapper.class);
        mapper.deleteProduto(id);
        session.commit();
        session.close();
    }
}
